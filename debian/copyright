Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Horde IMAP abstraction interface
Upstream-Contact: Horde <horde@lists.horde.org>
                  Horde development <dev@lists.horde.org>
Source: http://pear.horde.org/

Files: *
Copyright: 2004-2017, Horde LLC (http://www.horde.org/)
           2005-2017, Horde LLC (http://www.horde.org/)
           2009-2017, Horde LLC (http://www.horde.org/)
           2008-2017, Horde LLC (http://www.horde.org/)
           2011-2017, Horde LLC (http://www.horde.org/)
           2012-2013, Horde LLC (http://www.horde.org/)
           2012-2015, Horde LLC (http://www.horde.org/)
           2012-2017, Horde LLC (http://www.horde.org/)
           2013-2017, Horde LLC (http://www.horde.org/)
           2013, Horde LLC (http://www.horde.org/)
           2014, Horde LLC (http://www.horde.org/)
           2014-2017, Horde LLC (http://www.horde.org/)
           2015-2017, Horde LLC (http://www.horde.org/)
           2005-2012, Thomas Bruederli <roundcube@gmail.com>
License: LGPL-2.1

Files: Horde_Imap_Client-*/lib/Horde/Imap/Client/Utf7imap.php
Copyright: 2000, Edmund Grimley Evans <edmundo@rano.org>
           Thomas Bruederli <roundcube@gmail.com>
           2008-2017, Horde LLC (http://www.horde.org/)
License: LGPL-2.1 and GPL-2

Files: Horde_Imap_Client-*/lib/Horde/Imap/Client/Auth/DigestMD5.php
Copyright: 2002-2003, Richard Heyes <richard@phpguru.org>
           2011-2017 Horde LLC (http://www.horde.org/)
License: LGPL-2.1 and BSD-3-clause

Files: Horde_Imap_Client-*/lib/Horde/Imap/Client/Socket.php
Copyright: 2005-2017, Horde LLC (http://www.horde.org/)
           1999-2007, The SquirrelMail Project Team (http://squirrelmail.org/)
License: LGPL-2.1 and GPL-2+
Comment:
 SquirrelMail code portions must be considered as GPL-2+ code.
 .
 See http://squirrelmail.org/wiki/SquirrelMailGPL, no specific
 license version is given, code headers point to
 http://opensource.org/licenses/gpl-license.php
 which references GPL-2 and GPL-3, summarized on that page as
 GPL-2+.

Files: debian/*
Copyright: 2011-2019, Mathieu Parent <math.parent@gmail.com>
           2020, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: LGPL-2.1+ or BSD-3-clause or GPL-2+

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; version
 2 of the License.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 o Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 o Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 o The names of the authors may not be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
