php-horde-imap-client (2.30.6-3) unstable; urgency=medium

  * Team upload.
  * [322db63] d/patches: drop mixed type in List.php. (Closes: #1028424)

 -- Anton Gladky <gladk@debian.org>  Fri, 20 Jan 2023 20:34:18 +0100

php-horde-imap-client (2.30.6-2) unstable; urgency=medium

  * Team upload.
  * d/patches: fix type mismatch (Closes: #1028346).

 -- Anton Gladky <gladk@debian.org>  Tue, 10 Jan 2023 06:12:33 +0100

php-horde-imap-client (2.30.6-1) unstable; urgency=medium

  * Team upload.

  [ Mike Gabriel ]
  * New upstream version 2.30.6

  [ Anton Gladky ]
  * d/salsa-ci.yml: simplify rules.
  * d/patches: Fix most warnings due to PHP8.1.
  * d/tests/control: allow stderr.

 -- Anton Gladky <gladk@debian.org>  Sat, 24 Dec 2022 22:44:28 +0100

php-horde-imap-client (2.30.4-1) unstable; urgency=medium

  * New upstream version 2.30.4

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 08 Mar 2022 21:11:00 +0100

php-horde-imap-client (2.30.3-1) unstable; urgency=medium

  * New upstream version 2.30.3
  * d/patches: Adapt path names in patch files to new upstream version.
  * d/patches: Drop patches 0001 and 0002. Applied upstream.
  * d/control: Bump Standards-Version: to 4.6.0. No changes needed.
  * d/watch: Switch to format version 4.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 09 Dec 2021 14:45:25 +0100

php-horde-imap-client (2.30.1-4) unstable; urgency=medium

  * d/patches: Add 0001_fix-remove-redundant-array_diff.patch,
    0002_Fix-removing-FETCH-query-labels-in-PHP-8.patch. Prevent unwanted
    removal of mails. Prepare for PHP 8.x.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 05 Jul 2021 14:12:17 +0200

php-horde-imap-client (2.30.1-3) unstable; urgency=medium

  * d/t/control: Add Ds: php-horde-hashtable, php-horde-pack.
  * d/t/control: Add D: php-horde-cache and php-horde-db.
  * d/t/control: Assure that php-horde-test is (>= 2.6.4+debian0-6~).
  * d/patches: Drop 0001-s-PHPUnit_Framework_TestCase-Horde_Test_Case-g.patch.
  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/patches: Add 2001_no-IMAP+POP3-live-tests-during-Debian-builds.patch.
    Skip live tests during Debian builds.
  * d/t/phpunit: Drop manually amending phpunit.xml, now part of the 1010 patch.
  * d/t/phpunit: Unset -x bash flag (causing test failures).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 12 Sep 2020 19:52:01 +0000

php-horde-imap-client (2.30.1-2) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.

  [ Juri Grabowski ]
  * d/salsa-ci.yml: enable aptly
  * d/salsa-ci.yml: allow_failure on autopkgtest.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 10:43:49 +0200

php-horde-imap-client (2.30.1-1) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959298).

  * New upstream version 2.30.1
  * debian/copyright:
    + Fix license of Horde_Imap_Client-*/lib/Horde/Imap/Client/Utf7imap.php;
      drop comment.
    + Explain for Horde_Imap_Client-*/lib/Horde/Imap/Client/Socket.php about
      GPL-2+ portions in the code.
  * debian/patches:
    + Rebase 0001-s-PHPUnit_Framework_TestCase-Horde_Test_Case-g.patch.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 27 May 2020 14:30:11 +0200

php-horde-imap-client (2.29.17-3) REJECTED; urgency=medium

  * Re-upload to Debian. (Closes: #959298).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 07 May 2020 11:17:57 +0200

php-horde-imap-client (2.29.17-2) unstable; urgency=medium

  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:04:15 +0200

php-horde-imap-client (2.29.17-1) unstable; urgency=medium

  * New upstream version 2.29.17
    - Fix SORT and THREAD command when UTF8=ACCEPT (Closes: #939129)
  * Bump debhelper from old 11 to 12.
  * Add debian/salsa-ci.yml
  * s/PHPUnit_Framework_TestCase/Horde_Test_Case/g
  * Add phpunit.xml.dist
  * Use phpunit.xml.dist

 -- Mathieu Parent <sathieu@debian.org>  Mon, 16 Sep 2019 14:12:28 +0200

php-horde-imap-client (2.29.16-1) unstable; urgency=medium

  * New upstream version 2.29.16

 -- Mathieu Parent <sathieu@debian.org>  Thu, 25 Oct 2018 20:06:05 +0200

php-horde-imap-client (2.29.15-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 10:57:41 +0200

php-horde-imap-client (2.29.15-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Thu, 05 Apr 2018 16:11:45 +0200

php-horde-imap-client (2.29.15-1) unstable; urgency=medium

  * New upstream version 2.29.15

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Sep 2017 22:30:42 +0200

php-horde-imap-client (2.29.14-1) unstable; urgency=medium

  * New upstream version 2.29.14

 -- Mathieu Parent <sathieu@debian.org>  Tue, 01 Aug 2017 22:15:26 +0200

php-horde-imap-client (2.29.13-1) unstable; urgency=medium

  * New upstream version 2.29.13

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Jul 2017 21:41:25 +0200

php-horde-imap-client (2.29.12-1) unstable; urgency=medium

  * New upstream version 2.29.12

 -- Mathieu Parent <sathieu@debian.org>  Sun, 18 Dec 2016 21:26:04 +0100

php-horde-imap-client (2.29.10-1) unstable; urgency=medium

  * New upstream version 2.29.10

 -- Mathieu Parent <sathieu@debian.org>  Thu, 03 Nov 2016 21:43:46 +0100

php-horde-imap-client (2.29.9-1) unstable; urgency=medium

  * New upstream version 2.29.9

 -- Mathieu Parent <sathieu@debian.org>  Fri, 09 Sep 2016 15:02:41 +0200

php-horde-imap-client (2.29.8-1) unstable; urgency=medium

  * New upstream version 2.29.8

 -- Mathieu Parent <sathieu@debian.org>  Sat, 02 Jul 2016 21:32:11 +0200

php-horde-imap-client (2.29.7-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https
  * New upstream version 2.29.7

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 07:54:35 +0200

php-horde-imap-client (2.29.6-1) unstable; urgency=medium

  * New upstream version 2.29.6

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 11:48:33 +0100

php-horde-imap-client (2.29.5-3) unstable; urgency=medium

  * Fix debian/tests/control: php-sqlite -> php-sqlite3

 -- Mathieu Parent <sathieu@debian.org>  Sun, 20 Mar 2016 00:10:00 +0100

php-horde-imap-client (2.29.5-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Mar 2016 16:10:35 +0100

php-horde-imap-client (2.29.5-1) unstable; urgency=medium

  * New upstream version 2.29.5

 -- Mathieu Parent <sathieu@debian.org>  Wed, 03 Feb 2016 23:02:17 +0100

php-horde-imap-client (2.29.4-1) unstable; urgency=medium

  * New upstream version 2.29.4

 -- Mathieu Parent <sathieu@debian.org>  Thu, 10 Dec 2015 08:00:39 +0100

php-horde-imap-client (2.29.3-2) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Fri, 23 Oct 2015 07:06:05 +0200

php-horde-imap-client (2.29.3-1) unstable; urgency=medium

  * New upstream version 2.29.3

 -- Mathieu Parent <sathieu@debian.org>  Thu, 17 Sep 2015 04:24:38 +0200

php-horde-imap-client (2.29.1-2) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 00:33:05 +0200

php-horde-imap-client (2.29.1-1) unstable; urgency=medium

  * New upstream version 2.29.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 29 Jul 2015 06:41:34 +0200

php-horde-imap-client (2.29.0-1) unstable; urgency=medium

  * New upstream version 2.29.0

 -- Mathieu Parent <sathieu@debian.org>  Thu, 18 Jun 2015 16:29:42 +0200

php-horde-imap-client (2.28.1-1) unstable; urgency=medium

  * New upstream version 2.28.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 16 Jun 2015 22:03:59 +0200

php-horde-imap-client (2.28.0-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.28.0

 -- Mathieu Parent <sathieu@debian.org>  Mon, 04 May 2015 21:44:08 +0200

php-horde-imap-client (2.25.2-1) unstable; urgency=medium

  * New upstream version 2.25.2

 -- Mathieu Parent <sathieu@debian.org>  Tue, 21 Oct 2014 05:42:26 +0200

php-horde-imap-client (2.25.1-2) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 14:14:05 +0200

php-horde-imap-client (2.25.1-1) unstable; urgency=medium

  * New upstream version 2.25.1

 -- Mathieu Parent <sathieu@debian.org>  Thu, 25 Sep 2014 08:16:16 +0200

php-horde-imap-client (2.25.0-2) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 14:47:49 +0200

php-horde-imap-client (2.25.0-1) unstable; urgency=medium

  * New upstream version 2.25.0

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Sep 2014 23:15:44 +0200

php-horde-imap-client (2.24.2-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:34:22 +0200

php-horde-imap-client (2.24.2-1) unstable; urgency=medium

  * New upstream version 2.24.2

 -- Mathieu Parent <sathieu@debian.org>  Sun, 17 Aug 2014 21:55:50 +0200

php-horde-imap-client (2.24.0-1) unstable; urgency=medium

  * New upstream version 2.24.0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 Aug 2014 11:17:01 +0200

php-horde-imap-client (2.23.2-1) unstable; urgency=medium

  * New upstream version 2.23.2

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 Jul 2014 23:23:39 +0200

php-horde-imap-client (2.23.1-1) unstable; urgency=medium

  * New upstream version 2.23.1

 -- Mathieu Parent <sathieu@debian.org>  Thu, 19 Jun 2014 22:52:30 +0200

php-horde-imap-client (2.22.0-1) unstable; urgency=medium

  * New upstream version 2.22.0

 -- Mathieu Parent <sathieu@debian.org>  Mon, 09 Jun 2014 10:42:30 +0200

php-horde-imap-client (2.21.0-1) unstable; urgency=medium

  * New upstream version 2.21.0

 -- Mathieu Parent <sathieu@debian.org>  Wed, 04 Jun 2014 23:01:58 +0200

php-horde-imap-client (2.19.6-1) unstable; urgency=medium

  * New upstream version 2.19.6

 -- Mathieu Parent <sathieu@debian.org>  Sat, 17 May 2014 13:18:34 +0200

php-horde-imap-client (2.19.4-1) unstable; urgency=medium

  * New upstream version 2.19.4

 -- Mathieu Parent <sathieu@debian.org>  Fri, 25 Apr 2014 12:34:38 +0200

php-horde-imap-client (2.19.2-1) unstable; urgency=medium

  * New upstream version 2.19.2

 -- Mathieu Parent <sathieu@debian.org>  Sun, 13 Apr 2014 09:34:12 +0200

php-horde-imap-client (2.19.1-1) unstable; urgency=medium

  * New upstream version 2.19.1

 -- Mathieu Parent <sathieu@debian.org>  Fri, 28 Mar 2014 18:28:33 +0100

php-horde-imap-client (2.19.0-1) unstable; urgency=medium

  * New upstream version 2.19.0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 11 Mar 2014 21:14:22 +0100

php-horde-imap-client (2.18.6-1) unstable; urgency=medium

  * New upstream version 2.18.6

 -- Mathieu Parent <sathieu@debian.org>  Sat, 08 Mar 2014 09:34:31 +0100

php-horde-imap-client (2.18.2-1) unstable; urgency=medium

  * New upstream version 2.18.2

 -- Mathieu Parent <sathieu@debian.org>  Fri, 21 Feb 2014 06:10:28 +0100

php-horde-imap-client (2.18.1-1) unstable; urgency=medium

  * New upstream version 2.18.1

 -- Mathieu Parent <sathieu@debian.org>  Sat, 15 Feb 2014 13:44:41 +0100

php-horde-imap-client (2.17.1-1) unstable; urgency=medium

  * New upstream version 2.17.1

 -- Mathieu Parent <sathieu@debian.org>  Sat, 25 Jan 2014 19:19:57 +0100

php-horde-imap-client (2.16.2-1) unstable; urgency=low

  * New upstream version 2.16.2

 -- Mathieu Parent <sathieu@debian.org>  Sat, 30 Nov 2013 19:48:07 +0100

php-horde-imap-client (2.16.0-1) unstable; urgency=low

  * New upstream version 2.16.0

 -- Mathieu Parent <sathieu@debian.org>  Sat, 02 Nov 2013 17:05:41 +0100

php-horde-imap-client (2.15.5-1) unstable; urgency=low

  * New upstream version 2.15.5

 -- Mathieu Parent <sathieu@debian.org>  Tue, 22 Oct 2013 15:17:03 +0200

php-horde-imap-client (2.15.3-1) unstable; urgency=low

  * New upstream version 2.15.3

 -- Mathieu Parent <sathieu@debian.org>  Tue, 17 Sep 2013 22:47:56 +0200

php-horde-imap-client (2.14.0-1) unstable; urgency=low

  * New upstream version 2.14.0

 -- Mathieu Parent <sathieu@debian.org>  Thu, 29 Aug 2013 07:37:56 +0200

php-horde-imap-client (2.12.1-1) unstable; urgency=low

  * New upstream version 2.12.1

 -- Mathieu Parent <sathieu@debian.org>  Wed, 14 Aug 2013 20:05:54 +0200

php-horde-imap-client (2.11.6-1) unstable; urgency=low

  * New upstream version 2.11.6

 -- Mathieu Parent <sathieu@debian.org>  Mon, 24 Jun 2013 11:08:58 +0200

php-horde-imap-client (2.11.5-1) unstable; urgency=low

  * New upstream version 2.11.5

 -- Mathieu Parent <sathieu@debian.org>  Wed, 12 Jun 2013 21:10:40 +0200

php-horde-imap-client (2.11.4-1) unstable; urgency=low

  * New upstream version 2.11.4

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:22:56 +0200

php-horde-imap-client (2.10.1-1) unstable; urgency=low

  * Use pristine-tar
  * New upstream version 2.10.1

 -- Mathieu Parent <sathieu@debian.org>  Mon, 27 May 2013 11:00:30 +0200

php-horde-imap-client (2.9.1-1) unstable; urgency=low

  * New upstream version 2.9.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 14 May 2013 20:47:00 +0200

php-horde-imap-client (2.9.0-1) unstable; urgency=low

  * New upstream version 2.9.0

 -- Mathieu Parent <sathieu@debian.org>  Mon, 06 May 2013 15:50:50 +0200

php-horde-imap-client (2.8.2-1) unstable; urgency=low

  * New upstream version 2.8.2

 -- Mathieu Parent <sathieu@debian.org>  Wed, 24 Apr 2013 08:52:51 +0200

php-horde-imap-client (2.8.1-1) unstable; urgency=low

  * New upstream version 2.8.1

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 15:58:47 +0200

php-horde-imap-client (2.4.2-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:30:05 +0100

php-horde-imap-client (2.4.2-1) unstable; urgency=low

  * New upstream version 2.4.2

 -- Mathieu Parent <sathieu@debian.org>  Sun, 06 Jan 2013 20:29:34 +0100

php-horde-imap-client (2.4.1-1) unstable; urgency=low

  * New upstream version 2.4.1

 -- Mathieu Parent <sathieu@debian.org>  Fri, 04 Jan 2013 19:18:39 +0100

php-horde-imap-client (2.3.2-1) unstable; urgency=low

  * New upstream version 2.3.2

 -- Mathieu Parent <sathieu@debian.org>  Fri, 07 Dec 2012 20:07:36 +0100

php-horde-imap-client (2.2.3-1) unstable; urgency=low

  * New upstream version 2.2.3
  * Fixed watchfile
  * Updated Standards-Version to 3.9.3: no change
  * Updated copyright format URL
  * Updated debian/pearrc to install Horde apps in /usr/share/horde
    instead of /usr/share/horde4
  * Updated Vcs-* fields

 -- Mathieu Parent <sathieu@debian.org>  Sat, 01 Dec 2012 11:19:38 +0100

php-horde-imap-client (1.4.4-1) unstable; urgency=low

  * Horde_Imap_Client package
  * Initial packaging (Closes: #657362)
  * Copyright file by Soren Stoutner and Jay Barksdale

 -- Mathieu Parent <sathieu@debian.org>  Wed, 15 Feb 2012 08:34:49 +0100
